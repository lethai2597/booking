<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Deluxe Service Office</title>
    <style>body{font-family: 'Quicksand', sans-serif;}</style>
    <link rel="shortcut icon" href="{{ asset('img/icons/favicon.png') }}" type="image/x-icon"/>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
    <!-- Styles -->
    @stack('css')
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body class="mx-auto text-gray-800 bg-gray-200" style="max-width: 1920px;">
  <header class="text-gray-700 bg-white shadow-lg lg:flex lg:justify-between xl:px-20 lg:items-center">
    <div class="flex items-center justify-between px-4 py-1">
      <img class="h-10 m-1 lg:h-16" src="{{ asset('img/logo.png') }}" alt="Deluxe Logo">
      <a href="#" class="inline-block lg:hidden" onclick="mobileMenuToggle()">
        <svg class="w-6 h-6 fill-current" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="icon-shape" class="fill-current">
              <path d="M0,3 L20,3 L20,5 L0,5 L0,3 Z M0,9 L20,9 L20,11 L0,11 L0,9 Z M0,15 L20,15 L20,17 L0,17 L0,15 Z" id="Combined-Shape"></path>
            </g>
          </g>
        </svg>
      </a>
    </div>
    <ul id="mobile-menu" class="h-0 overflow-hidden text-xl lg:h-full">
      <li class="p-4 lg:py-2 lg:inline-block"><a href="/">Trang chủ</a></li>
      <li class="p-4 lg:py-2 lg:inline-block"><a href="#overview">Giới thiệu</a></li>
      <li class="p-4 lg:py-2 lg:inline-block"><a  href="#services">Sản phẩm & dịch vụ</a></li>
      <li class="p-4 lg:py-2 lg:inline-block"><a href="#community">Cộng đồng</a></li>
      <li class="p-4 lg:py-2 lg:inline-block"><a  href="#booking">Liên hệ</a></li>
    </ul>
  </header>
  @yield('content')
  <footer class="text-gray-300 bg-green-800 border-t-4 border-green-900 lg:py-10">
    <div class="container flex flex-wrap mx-auto">
      <div class="w-full lg:w-2/5">
        <div class="p-4">
          <img class="h-24" src="{{ asset('img/logo1.png') }}" alt="">
          <p class="mb-4">
              Tầng 19 tòa nhà TNR, 54A Nguyễn Chí Thanh
              <br>Quận Ba Đình, Hà Nội, Việt Nam
            </p>
        </div>
      </div>
      <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/5">
        <div class="p-4 ml-10 sm:ml-0">
          <h3 class="mb-4 text-xl">LIÊN HỆ</h3>
          <ul class="">
            <a rel="noopener noreferrer" target="_blank" href="https://www.google.com/maps/place/54A+Nguy%E1%BB%85n+Ch%C3%AD+Thanh,+L%C3%A1ng+Th%C6%B0%E1%BB%A3ng,+%C4%90%E1%BB%91ng+%C4%90a,+H%C3%A0+N%E1%BB%99i,+Vi%E1%BB%87t+Nam/">
                <li class="flex items-center mb-5">
                    <i class="mr-1 text-2xl lar la-map"></i> Xem bản đồ 
                </li>
            </a>
            <a href="tel:+84942871166">
                <li class="flex items-center mb-5">
                    <i class="mr-1 text-2xl las la-phone"></i> +84 942 87 1166
                </li>
            </a>
            <a href="mailto:info@deluxeoffice.vn">
                <li class="flex items-center mb-5">
                    <i class="mr-1 text-2xl las la-envelope"></i> info@deluxeoffice.vn
                </li>
            </a>
            <a rel="noopener noreferrer" target="_blank" href="#">
                <li class="flex items-center mb-5">
                    <i class="mr-1 text-2xl lab la-facebook-square"></i> Deluxe fanpage
                </li>
            </a>
          </ul>
        </div>
      </div>
      <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/5">
        <div class="p-4 ml-10 sm:ml-0">
            <h3 class="mb-4 text-xl">LIÊN KẾT</h3>
            <ul class="ml-6 list-disc">
              <li class="mb-2">
                <a href="">Giới thiệu</a>
              </li>
              <li class="mb-2">
                <a href="">Dịch vụ</a>
              </li>
              <li class="mb-2">
                <a href="">Điều khoản</a>
              </li>
              <li class="mb-2">
                <a href="">Chính sách</a>
              </li>
              <li class="mb-2">
                <a href="">Liên hệ</a>
              </li>
            </ul>
          </div>
      </div>
      <div class="w-full md:w-1/3 lg:w-1/5">
        <div class="p-4">
          <h3 class="mb-4 text-xl">ĐĂNG KÝ</h3>
          <form action="https://docs.google.com/forms/u/0/d/e/1FAIpQLSdxZfwKdnjbwOY9BSjaOl2YUX2IZTiOQIAjFjUdeVHkBwJZCg/formResponse">
            <input name="entry.32147073" type="text" value="" class="w-full p-3 mb-2 text-gray-800 border rounded" placeholder="Địa chỉ Email">
            <button type="submit" class="w-full px-6 py-3 mb-2 font-bold text-white bg-green-700 border border-green-700 rounded shadow-md">Gửi Email</button>
          </form>
          <p class="text-sm text-center">Địa chỉ email sẽ được bảo mật.</p>
        </div>
      </div>
    </div>
  </footer>
  <script>
    function mobileMenuToggle(){
      document.getElementById("mobile-menu").classList.toggle("h-full");
    }
  </script>
  @stack('js')
</body>
</html>
