@extends('layouts.app')
@section('content')
<div class="bg-white">
    <section id="overview" data-slideshow="slideshow" class="relative w-full">
        <div class="overflow-hidden" data-glide-el="track">
        <ul class="relative flex flex-no-wrap w-full overflow-hidden">
          <li class="w-full h-full ">
            <img src="{{asset('img/bia1.jpg')}}" alt="Tổng quan về Deluxe">
          </li>
          <li class="flex-shrink-0 w-full h-full whitespace-normal">
            <img src="{{asset('img/bia2.jpg')}}" alt="Tổng quan về Deluxe">
          </li>
          <li class="flex-shrink-0 w-full h-full whitespace-normal">
            <img src="{{asset('img/bia3.jpg')}}" alt="Tổng quan về Deluxe">
          </li>
          <li class="flex-shrink-0 w-full h-full whitespace-normal">
            <img src="{{asset('img/bia4.jpg')}}" alt="Tổng quan về Deluxe">
          </li>
          <li class="flex-shrink-0 w-full h-full whitespace-normal">
            <img src="{{asset('img/bia5.jpg')}}" alt="Tổng quan về Deluxe">
          </li>
        </ul>
      </div>
      <div class="absolute top-0 right-0 hidden mt-10 mr-10 text-4xl text-green-800 bg-white lg:block" data-glide-el="controls">
        <button class="block w-16 h-16 border-b" data-glide-dir="<"><i class="las la-angle-left"></i></button>
        <button class="block w-16 h-16" data-glide-dir=">"><i class="las la-angle-right"></i></button>
      </div>
    </section>
    <section id="overview" class="p-6 md:p-12 xl:p-20 lg:flex">
        <div class="w-full mb-10 lg:mb-0 lg:w-8/12 md:mr-10 xl:ml-10">
            <h1 class="mb-6 text-4xl xl:text-5xl xl:mt-10">
                KHÔNG GIAN CHIA SẺ HIỆN ĐẠI NGAY KHU TRUNG TÂM HÀ NỘI
            </h1>
            <p class="leading-loose text-justify md:text-xl">
                "Trong thời đại ngày càng nhiều các startup, Office share đang trở thành xu hướng văn phòng cho các doanh nghiệp. Các công ty, doanh nghiệp ngày càng chú ý hơn đến vị trí kinh doanh của mình với mong muốn có một không gian <b>thuận lợi, sang trọng, kết nối</b> để phát triển trong tương lai. <br><br>
                Deluxe Service Office là chuỗi văn phòng, chỗ ngồi linh hoạt hướng đến một cộng đồng những người có cùng mục tiêu, cùng chí hướng để phát triển công việc. Tọa lạc tại khu tòa nhà văn phòng hạng A - TNR Tower Nguyễn Chí Thanh, được ví như miền đất lành để tạo nên một mạng lưới doanh nghiệp phát triển và đẳng cấp. <br><br>
                Với không gian xanh mát, hệ thống các phòng riêng và chỗ ngồi đa dạng, tiện nghi, vị trí đắc địa và giao thông thuận lợi chắc chắn sẽ là một bước ghi điểm tuyệt vời mỗi khi bạn cần kết nối với khách hàng, đối tác. Một môi trường làm việc tập trung và đa dạng thương hiệu sẽ là bước khởi đầu hoàn hảo cho những dự án lớn của công ty."<br><br>
            </p>
        </div>
        <div class="w-full max-w-md mx-auto lg:w-5/12">
            <div class="p-4 mt-6 border shadow-md lg:p-10">
                <img class="mx-auto" src="{{ asset('img/logosidebar.png') }}" alt="Deluxe Service Office">
                <h2 class="my-4 text-2xl font-bold">
                    Deluxe Service Office
                </h2>
                <p class="my-4 mb-4 leading-noose">
                    Tầng 19 tòa nhà TNR <br>
                    54A Nguyễn Chí Thanh <br>
                    Quận Ba Đình <br>
                    Hà Nội <br>
                    Việt Nam <br>
                </p>
                <ul class="my-6">
                    <a rel="noopener noreferrer" target="_blank" href="https://www.google.com/maps/place/54A+Nguy%E1%BB%85n+Ch%C3%AD+Thanh,+L%C3%A1ng+Th%C6%B0%E1%BB%A3ng,+%C4%90%E1%BB%91ng+%C4%90a,+H%C3%A0+N%E1%BB%99i,+Vi%E1%BB%87t+Nam/">
                        <li class="flex items-center mb-4">
                            <i class="mr-1 text-2xl text-gray-700 lar la-map"></i> Xem bản đồ 
                        </li>
                    </a>
                    <a href="tel:+84942871166">
                        <li class="flex items-center mb-4">
                            <i class="mr-1 text-2xl text-gray-700 las la-phone"></i> +84 942 87 1166
                        </li>
                    </a>
                    <a href="mailto:info@deluxeoffice.vn">
                        <li class="flex items-center mb-4">
                            <i class="mr-1 text-2xl text-gray-700 las la-envelope"></i> info@deluxeoffice.vn
                        </li>
                    </a>
                </ul>
                <a href="#booking" class="block px-6 py-3 my-4 font-bold text-center text-white bg-green-700 rounded hover:bg-green-800">ĐẶT LỊCH</a>
            </div>
        </div>
    </section>
    <section>
    <div class="m-6 text-center border md:p-6 md:m-12 lg:m-20 md:p-12 lg:p-20">
        <h3 class="my-10 text-3xl font-bold xl:text-5xl lg:mt-0">
            Những lợi ích tại Deluxe
        </h3>
        <ul class="flex flex-wrap text-center">
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-map-marked-alt"></i><br>
                Vị trí trung tâm
            </li>
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-city"></i><br>
                Không gian hạng A
            </li>
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-door-open"></i><br>
                Lễ tân chuyên nghiệp
            </li>
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-wine-glass"></i><br>
                Trà, cafe miễn phí
            </li>
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-chair"></i><br>
                50 Chỗ ngồi linh hoạt
            </li>
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-tablet"></i><br>
                18 Văn phòng làm việc riêng
            </li>
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-tv"></i><br>
                3 Phòng họp
            </li>
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-archway"></i><br>
                Sảnh sự kiện 150 người
            </li>
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-wifi"></i><br>
                Wi-Fi tốc độ cao
            </li> 
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-phone-volume"></i><br>
                Điện thoại phòng riêng
            </li>
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-leaf"></i><br>
                Công trình xanh
            </li>
            <li class="w-1/2 px-4 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                <i class="mb-2 text-4xl text-gray-700 las la-handshake"></i><br>
                Kết nối cộng đồng
            </li>
        </ul>
    </div>
    </section>
    <section id="services" class="p-6 bg-gray-100 md:p-12 xl:p-20">
        <h2 class="mb-6 text-4xl font-bold text-center">
            Một không gian làm việc đáp ứng mọi nhu cầu.
        </h2>
        <h4 class="mb-4 text-xl font-bold text-center">
            Xem các tùy chọn dành cho bạn và doanh nghiệp của bạn.
        </h4>
        <div class="mt-10 md:flex lg:mt-20 md:flex-wrap">
            <div class="flex w-full max-w-xl mx-auto md:w-1/2 lg:w-1/4">
                <div class="flex flex-col justify-between m-2 bg-white rounded hover:shadow-md">
                    <div>
                        <img src="{{ asset('img/VP.jpg') }} " alt="VĂN PHÒNG KHÉP KÍN">
                        <div class="px-8 py-4">
                            <h5 class="mb-4 font-bold text 3xl">
                                VĂN PHÒNG KHÉP KÍN
                            </h5>
                            <div>
                                <p class="text-sm text-gray-500 ">
                                    <span class="text-2xl font-bold text-black xl:text-3xl"> Từ 21,600,000 <br>VND </span>/tháng
                                </p>
                            </div>
                            
                        </div>
                    </div>
                    <div class="px-8 py-4">
                        <a href="#booking" class="block p-3 text-center text-white bg-green-800 rounded hover:bg-green-900">Đặt chỗ ngay</a>
                    </div>
                </div> 
            </div>
            <div class="flex w-full max-w-xl mx-auto md:w-1/2 lg:w-1/4">
                <div class="flex flex-col justify-between m-2 bg-white rounded hover:shadow-md">
                    <div>
                        <img src="{{ asset('img/CD.jpg') }} " alt="CHỖ NGỒI CỐ ĐỊNH">
                        <div class="px-8 py-4">
                            <h5 class="mb-4 font-bold text 3xl">
                                CHỖ NGỒI CỐ ĐỊNH
                            </h5>
                            <div>
                                <p class="text-sm text-gray-500 ">
                                    
                                    <span class="text-2xl font-bold text-black xl:text-3xl"> 4,500,000 <br>VND</span>
                                    /tháng
                                </p>
                            </div> 
                        </div>
                    </div>
                    <div class="px-8 py-4">
                        <a href="#booking" class="block p-3 text-center text-white bg-green-800 rounded hover:bg-green-900">Đặt chỗ ngay</a>
                    </div>
                </div>
            </div>
            <div class="flex w-full max-w-xl mx-auto md:w-1/2 lg:w-1/4">
                <div class="flex flex-col justify-between m-2 bg-white rounded hover:shadow-md">
                    <div>
                        <img src="{{ asset('img/LH.jpg') }} " alt="CHỖ NGỒI LINH HOẠT">
                        <div class="px-8 py-4">
                            <h5 class="mb-4 font-bold text 3xl">
                                CHỖ NGỒI LINH HOẠT
                            </h5>
                            <div>
                                <p class="text-sm text-gray-500 ">
                                    
                                    <span class="text-2xl font-bold text-black xl:text-3xl"> 2,800,000 <br>VND</span>
                                    /tháng
                                </p>
                            </div>   
                        </div>
                    </div>
                    <div class="px-8 py-4">
                        <a href="#booking" class="block p-3 text-center text-white bg-green-800 rounded hover:bg-green-900">Đặt chỗ ngay</a>
                    </div>
                </div>
            </div>  
            <div class="flex w-full max-w-xl mx-auto md:w-1/2 lg:w-1/4">
                <div class="flex flex-col justify-between m-2 bg-white rounded hover:shadow-md">
                    <div>
                        <img src="{{ asset('img/PH.jpg') }} " alt="PHÒNG HỌP & KHÔNG GIAN HỘI TRƯỜNG">
                        <div class="px-8 py-4">
                            <h5 class="mb-4 font-bold text 3xl">    
                                PHÒNG HỌP & EVENT
                            </h5>
                            <div>
                                <p class="text-sm text-gray-500 ">
                                    
                                    <span class="text-2xl font-bold text-black xl:text-3xl"> Từ 800,000 <br>VND</span>
                                    /giờ
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="px-8 py-4">
                        <a href="#booking" class="block p-3 text-center text-white bg-green-800 rounded hover:bg-green-900">Đặt chỗ ngay</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="booking" class="p-6 bg-gray-100 md:p-12 lg:p-20">
        <div class="max-w-3xl mx-auto text-center text-gray-800">
            <h2 class="max-w-xl mx-auto mb-4 text-4xl font-bold">Book ngay một ngày trải nghiệm Free cùng Deluxe</h2>
            <ul class="flex flex-wrap mb-4 text-center">
                <li class="w-1/2 px-2 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                    <i class="mb-2 text-4xl text-gray-700 las la-city"></i><br>
                    Không gian hạng A
                </li>
                <li class="w-1/2 px-2 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                    <i class="mb-2 text-4xl text-gray-700 las la-door-open"></i><br>
                    Lễ tân chuyên nghiệp
                </li>
                <li class="w-1/2 px-2 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                    <i class="mb-2 text-4xl text-gray-700 las la-wifi"></i><br>
                    Wi-Fi tốc độ cao
                </li>
                <li class="w-1/2 px-2 py-6 transition duration-500 cursor-pointer hover:bg-gray-100 md:w-1/3 lg:w-1/4">
                    <i class="mb-2 text-4xl text-gray-700 las la-wine-glass"></i><br>
                    Trà, cafe miễn phí
                </li>
            </ul>
            <form action="https://docs.google.com/forms/u/0/d/e/1FAIpQLSeZF_binoLz9z6nkQvb9deBM2gGaRf8QnFP0yFtAUjGhLDmjA/formResponse">
                <div class="max-w-xl mx-auto text-left">
                        <div class="flex flex-wrap">
                            <div class="w-full mx-2 my-4">
                                <label for="name" class="block mb-3 font-bold text-gray-600">
                                    Họ tên (*)
                                </label>
                                <input id="name" name="entry.1572942433" type="text" value="" required="" class="w-full p-3 border rounded ">
                            </div>
                            <div class="w-full mx-2 my-4">
                                <label for="email" class="block mb-3 font-bold text-gray-600">
                                    Email
                                </label>
                                <input id="email" name="entry.1629235157" type="text" value="" class="w-full p-3 border rounded ">
                            </div>
                        </div>
                        <div class="flex">
                            <div class="w-full mx-2 my-4">
                                <label for="phone" class="block mb-3 font-bold text-gray-600">
                                Số điện thoại (*)
                                </label>
                                <input id="phone" name="entry.211742373" type="text" value="" required="" class="w-full p-3 border rounded ">
                            </div>
                        </div>
                        <div class="flex">
                            <div class="w-full mx-2 my-4">
                                <label for="content" class="block mb-3 font-bold text-gray-600">
                                Ghi chú
                                </label>
                                <textarea name="entry.1910835190" id="content" cols="30" rows="5" class="w-full p-3 border rounded "></textarea>
                            </div>
                        </div>
                        <div class="flex">
                            <div class="w-full mx-2 my-4">
                                <button type="submit" class="w-full px-6 py-3 font-bold text-white bg-green-800 rounded hover:bg-green-900">
                                    Gửi liên hệ
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </form>
        </div>
    </section>
    <section class="bg-green-800 bg-cover lg:flex " style="background-image: linear-gradient( 135deg, #368c63 10%, #113323 100%);">
        <div class="w-full p-10 text-white lg:w-1/2 xl:w-5/12">
            <h2 class="mb-4 text-3xl font-bold xl:mb-10">Lý do bạn nên chọn Deluxe Service Office</h2>
            <ul class="ml-4 text-sm list-disc lg:text-base ">
                <li>Môi trường làm việc chuyên nghiệp, cơ sở vật chất hiện đại và cao cấp. <br><br></li>
                <li>Địa điểm đắc địa, nằm tại tầng 19 tòa nhà hạng A, TNR tower, Số 54A Nguyễn Chí Thanh, Ba Đình, Hà Nội. Nơi tập trung nhiều doanh nghiệp, ngân hàng lớn. Giao thông thuận lợi. <br><br></li>
                <li>Văn phòng riêng biệt với thiết kế tối giản nhưng không kém phần sang trọng và hiện đại với điểm nhấn là các góc nhìn khung cảnh toàn thành phố.<br><br></li>
                <li>Toàn bộ chỗ ngồi linh hoạt được thiết kế trong không gian hiện đại, bao quanh bởi những cây xanh siêu đẹp tạo cảm hứng sáng tạo và thoải mái làm việc. <br><br></li>
                <li> Không gian hội thảo lớn với sức chứa từ 50 đến 150 người."</li>
            </ul>
        </div>
        <div class="flex items-center w-full lg:w-1/2 xl:w-7/12">
            <iframe class="w-full" height="500" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.230576215808!2d105.80718991547958!3d21.02345814333381!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab689b1045cd%3A0x23804386ee1d8d5d!2zNTRBIE5ndXnhu4VuIENow60gVGhhbmgsIEzDoW5nIFRoxrDhu6NuZywgxJDhu5FuZyDEkGEsIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1583047884600!5m2!1svi!2s" allowfullscreen=""></iframe>
        </div>
    </section>
    <section id="community" class="p-6 md:p-12 lg:p-20">
        <div class="mb-6 lg:flex">
            <img class="w-full mt-10 lg:mt-0 lg:pr-10 lg:w-7/12" src="{{ asset('img/about-vp.jpg') }}" alt="Bạn được sở hữu không gian văn phòng với hợp đồng linh hoạt">
            <div class="flex items-center">
                <h2 class="p-6 text-3xl bg-white lg:text-4xl xl:text-5xl lg:-ml-32">
                    Bạn được sở hữu không gian văn phòng với hợp đồng linh hoạt. 
                </h2>
            </div>
        </div>
        <div class="text-justify md:flex">
            <p class="p-6">
                Một số doanh nhân <b>làm việc hiệu quả hơn</b> khi sở hữu không gian văn phòng riêng. Một nơi bạn có thể đăng kế hoạch <b>thống lĩnh thị trường</b> thế giới mà không bị người khác dòm ngó. 
            </p>
            <p class="p-6">
                Bất kể bạn cần không gian văn phòng có diện tích chỉ một vài hay lên tới vài trăm mét vuông thì Deluxe có thể cung cấp cho bạn một môi trường có diện tích <b>hoàn toàn phù hợp</b> với bạn và nhóm của bạn. Bạn có thể bố trí không gian theo cách phù hợp với mình và ổn định chỗ ngồi ở đó, ra vào không gian an toàn 24/7 – và gia nhập <b></b>mạng lưới doanh nghiệp trong giờ làm việc thông thường. 
            </p>
            <p class="p-6">
                Vì hiểu rằng công việc kinh doanh có thể thay đổi vô cùng nhanh nên chúng tôi không bao giờ yêu cầu bạn phải ký hợp đồng dài hạn – các <b>điều khoản hợp đồng</b> của chúng tôi rất linh hoạt và được điều chỉnh cho phù hợp với nhu cầu cụ thể của bạn.
            </p>
        </div>
    </section>
    <section class="p-6 bg-gray-100 md:p-12 lg:p-20">
        <div class="flex-row-reverse mb-6 lg:flex">
            <img class="w-full mt-10 lg:mt-0 lg:pr-10 lg:w-7/12" src="{{ asset('img/about-congdong.jpg') }}" alt="Gặp gỡ, giao lưu với đối tác mới mà không cần rời khỏi văn phòng">
            <div class="flex items-center ">
                <h2 class="p-6 text-3xl bg-white bg-gray-100 lg:text-4xl xl:text-5xl lg:-mr-32">
                    Gặp gỡ, giao lưu với đối tác mới mà không cần rời khỏi văn phòng.
                </h2>
            </div>
        </div>
        <div class="text-justify md:flex">
            <p class="p-6">
                Lựa chọn một <b>vị trí mới</b> trong mạng lưới doanh nghiệp, không gian làm việc được thiết kế đẹp mắt nhằm tạo điều kiện cho mọi người tạo dựng các mối quan hệ mới. 
            </p>
            <p class="p-6">
                Cho dù bạn lập bản đồ tư duy, họp, cộng tác hay gặm nhấm các con số thì những chiếc bàn làm việc được thiết kế chuyên biệt, ghế sofa, Wi-Fi cực nhanh và khu vực họp thân thiện là <b>một nơi hoàn hảo</b> phù hợp với mọi nhu cầu công việc .  
            </p>
            <p class="p-6">
                Vì vậy, nếu bạn muốn có một chiếc bàn làm việc yên tĩnh vào Thứ Hai để hoàn thiện bản kế hoạch chi tiết, một chiếc bàn lớn vào Thứ Ba để ngồi điều phối nhóm của mình, một chiếc bàn ăn trưa vào Thứ Tư để gặp gỡ khách hàng mới, một góc ấm cúng để nhâm nhi cà phê trong những ngày còn lại trong tuần để sàng lọc ứng viên tiềm năng — <b>Deluxe</b> luôn có tất cả các không gian mà bạn cần, mở cửa toàn thời gian, 5 ngày hoặc 10 ngày trong một tháng.
            </p>
        </div>
    </section>
    <section class="p-6 md:p-12 lg:p-20">
        <div class="mb-6 lg:flex">
            <img class="w-full mt-10 lg:mt-0 lg:pr-10 lg:w-7/12" src="{{ asset('img/about-dichvu.jpg') }}" alt="Bạn có tất cả những gì bạn cần mà chỉ phải trả một giá phí">
            <div class="flex items-center ">
                <h2 class="p-6 text-3xl bg-white lg:text-4xl xl:text-5xl lg:-ml-32">
                    Bạn có tất cả những gì bạn cần mà chỉ phải trả một giá phí. 
                </h2>
            </div>
        </div>
        <div class="text-justify md:flex">
            <p class="p-6">
                Khi gia nhập Deluxe, bạn sẽ có <b>mọi thứ mà bạn cần</b> cho công việc - tất cả ở một nơi, bao gồm trong <b>một giá phí duy nhất</b>. Mọi thứ luôn sẵn sàng, từ Wi-Fi cực nhanh cho tới nội thất kiểu cách.
            </p>
            <p class="p-6">
                Đó là còn chưa kể đến <b>cộng đồng doanh nhân</b> có óc sáng tạo muốn gặp gỡ, giao lưu với bạn. Chúng tôi sẽ lo liệu mọi công việc nhàm chán như điện nước và vệ sinh – tất cả đều đã bao gồm trong một giá phí duy nhất.
            </p>
            <p class="p-6">
                Khi bạn muốn tổ chức một cuộc họp, sao gấp một vài tài liệu hoặc nghỉ ngơi thư giãn với một ly cà phê, bạn chỉ phải thanh toán riêng các dịch vụ này. Vì vậy, bạn <b>luôn biết được mình đang đứng ở đâu</b>. Điều ngạc nhiên duy nhất là người đang đứng cạnh bạn.
            </p>
        </div>
    </section>
</div>
@endsection
@push('js')
<script src="{{ asset('js/glide.min.js') }}"></script>
<script type="text/javascript">
new Glide('[data-slideshow=slideshow]', {
    type: 'carousel',
    autoplay: 3000,
    perView: 1,
}).mount();
</script>
@endpush